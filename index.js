require('dotenv').config()

const Discord = require('discord.js')
const _ = require('lodash')
const moment = require('moment')

const client = new Discord.Client()

function date (date) {
  return moment(date, 'MM/DD/YYYY')
}

// Place new schedule data here.
const scheduleData = [{
  date: date('12/07/18'),
  activities: [{
    type: 'Scrim',
    time: '8:00pm',
  }, {
    type: 'Scrim',
    time: '10:00pm',
  }],
}, {
  date: date('12/08/18'),
  activities: [{
    type: 'VOD Review',
    time: '6:00pm',
  }, {
    type: 'Scrim',
    time: '8:00pm',
  }],
}]

// Print the schedule to the DFC server #schedule channel.
function printSchedule (channel, schedule) {
  // Loop through each day.
  schedule.forEach(day => {
    let messageToSend = `**${day.date.format('dddd')}** - ${day.date.format('MMMM Do')}`
    // Display all activities and their time.
    day.activities.forEach(activity => {
      messageToSend += `\n - ${activity.type} @ ${activity.time} EST`
    })
    // If no activities are scheduled for this day, it's a day off.
    if (!day.activities || day.activities.length === 0) {
      messageToSend += '\n - Day Off'
    }
    channel.send(messageToSend)
  })
}

// Login
client.login(process.env.LOGIN_TOKEN)
  
// After we are 'ready', print the schedule.
client.on('ready', () => {
  let channel = client.channels.find(val => val.id === process.env.CHANNEL_ID)
  printSchedule(channel, scheduleData)
})
